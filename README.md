# JBake Template

---

Example [JBake](https://jbake.org/) website using GitLab Pages. 
View it live on the pages of gitlab. For:

- framagit : http://\[name\].frama.io/\[project_name\]
- gitlab: http://\[name\].gitlab.io/\[project_name\]

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).

---

## Getting Started

You can get started with Pages using JBake easily by either forking this repository or by uploading a new/existing JBake project.

Remember you need to wait for your site to build before you will be able to see your changes.  
You can track the build on the **Pipelines** tab.


### Start by forking this repository

1. Fork this repository.
1. **IMPORTANT:** Remove the fork relationship.
Go to **Settings (⚙)** > **Edit Project** and click the **"Remove fork relationship"** button.
1. Enable Shared Runners.
Go to **Settings (⚙)** > **Pipelines** and click the **"Enable shared Runners"** button.
1. Rename the repository to match the name you want for your site.
1. Edit your website through FramaGit/GitLab or clone the repository and push your changes.

### Start from a local JBake project

1. Add [this `.gitlab-ci.yml`](.gitlab-ci.yml) to the root of your project.
1. Push your repository and changes to FramaGit/GitLab.


## Public GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: fasar/jbake:latest
 

pages:
  stage: deploy
  script:
    - jbake -b /site /site/public
  artifacts:
    paths:
      - public
  only:
    - master
```

## Private Gitlab

If you have a private GitLab instance and you don't have image system (docker) initialized, you can use the following `.gitlab-ci-yml`

```
pages:
  stage: deploy
  script:
      - ./gradlew 
  artifacts:
    paths:
      - public
  only:
    - master
```



